﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FastFood.Data;
using FastFood.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace FastFood.Controllers
{
    public class CafeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly UserManager<ApplicationUser> _userManager;

        public CafeController(ApplicationDbContext context, IHostingEnvironment environment, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _environment = environment;
            _userManager = userManager;
        }

        // GET: Cafe
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Cafes.Include(c => c.User);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Cafe/Details/5
        public async Task<IActionResult> Details(int? id, List<Photo> photos)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var cafe = await _context.Cafes
            //    .Include(c => c.User)
            //    .SingleOrDefaultAsync(m => m.Id == id);
            //if (cafe == null)
            //{
            //    return NotFound();
            //}

            //return View(cafe);
            
            Cafe cafe = _context.Cafes.Include(t => t.Reviews).Include(t => t.User).FirstOrDefault(t => t.Id == id);
            cafe.Photos = _context.Photos.Where(i => i.CafeId == id).ToList();
            CafeDetailsViewModel model = new CafeDetailsViewModel()
            {
                Cafe = cafe,
                ReviewsViewModel = new ReviewsViewModel()
                {
                    CafeId = cafe.Id,
                    Reviews = cafe.Reviews,
                    NewReview = new Review() { CafeId = cafe.Id }
                }
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Review(int cafeId, string reviewText)
        {
            Review review = new Review();
            review.ReviewDate = DateTime.Now;
            Cafe cafe = _context.Cafes.FirstOrDefault(t => t.Id == cafeId);
            cafe.Review++;
            _context.Cafes.Update(cafe);
            review.Cafe = cafe;
            review.ReviewText = reviewText;
            _context.Reviews.Add(review);
            _context.SaveChanges();
            return Json(reviewText);
        }

        // GET: Cafe/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: Cafe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Rating,UserId")] Cafe cafe, List<IFormFile> cafeFiles)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cafe);
                await _context.SaveChangesAsync();
                UploadFiles(cafe, cafeFiles);
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", cafe.UserId);
            return View(cafe);
        }


        public void UploadFiles(Cafe cafe, List<IFormFile> cafeFiles)
        {
            if (cafeFiles != null)
            {
                foreach (var element in cafeFiles)
                {
                    var fileName = Path.Combine(_environment.WebRootPath + "/images", Path.GetFileName(element.FileName));
                    element.CopyTo(new FileStream(fileName, FileMode.Create));
                    Photo photo = new Photo() { Location = fileName, UserId = cafe.UserId, CafeId = cafe.Id, Name = element.FileName };
                    _context.Photos.Add(photo);
                    _context.SaveChanges();
                }
            }
        }



        // GET: Cafe/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cafe = await _context.Cafes.SingleOrDefaultAsync(m => m.Id == id);
            if (cafe == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", cafe.UserId);
            return View(cafe);
        }

        // POST: Cafe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Rating,UserId")] Cafe cafe)
        {
            if (id != cafe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cafe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CafeExists(cafe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", cafe.UserId);
            return View(cafe);
        }

        // GET: Cafe/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cafe = await _context.Cafes
                .Include(c => c.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cafe == null)
            {
                return NotFound();
            }

            return View(cafe);
        }

        // POST: Cafe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cafe = await _context.Cafes.SingleOrDefaultAsync(m => m.Id == id);
            _context.Cafes.Remove(cafe);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CafeExists(int id)
        {
            return _context.Cafes.Any(e => e.Id == id);
        }
    }
}
