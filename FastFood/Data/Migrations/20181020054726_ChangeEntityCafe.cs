﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FastFood.Data.Migrations
{
    public partial class ChangeEntityCafe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Cafes");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Cafes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Cafes");

            migrationBuilder.AddColumn<double>(
                name: "Rating",
                table: "Cafes",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
