﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace FastFood.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public List<Review> Reviews { get; set; }
        public List<Cafe> Cafes { get; set; }
        public List<Photo> Photos { get; set; }
    }
}
