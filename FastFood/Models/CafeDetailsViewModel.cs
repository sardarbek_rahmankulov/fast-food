﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FastFood.Models
{
    public class CafeDetailsViewModel
    {
        public Cafe Cafe { get; set; }
        public ReviewsViewModel ReviewsViewModel { get; set; }

        public SelectList RatingList { get; set; }
        [Range(0, 5)]
        public double Rating { get; set; }
    }
}
