﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood.Models
{
    public class ReviewsViewModel
    {
        public int CafeId { get; set; }
        public List<Review> Reviews { get; set; }
        public Review NewReview { get; set; }
    }
}
