﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
    }
}
