﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood.Models
{
    public class Cafe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainPhotoLocation { get; set; }
        public double Rating { get; set; }
        public int Review { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public List<Review> Reviews { get; set; }
        public List<Photo> Photos { get; set; }
    }
}
