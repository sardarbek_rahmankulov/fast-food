﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string ReviewText  { get; set; }
        public DateTime ReviewDate { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
    }
}
